// import java.io.BufferedReader;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.io.InputStreamReader;
// import java.io.PrintWriter;

import java.io.* ;

public class Dame8 {
	
	static int tab[] = new int[9];
	static int limit=300;
	static int compteur=0;
	static boolean debug=false;
	static float deltacol=0;
	static float deltaligne=0;
	static int sol=0;
	static String solFileName = "damesol.txt";
	static PrintWriter out; 
	
	
	//static String className = this.getClass().getName();
	
	public static boolean conflit(int lignevar,int colvar) {
	int i=0;
	int coltab,lignetab;
	float pente;
	boolean myconflit=false;
	
	if (debug) System.out.println("\n CONFLIT IN lignevar "+lignevar+" COLVAR "+colvar);
	
		for (i=1;i<9;i++){
		
			lignetab = i;
			coltab = tab[i];
			
			if (debug) System.out.println("conflit check lignevar:"+lignevar+" colvar:"+colvar+" LIGNETAB:"+lignetab+ " COLTAB:"+coltab);
			
			if (tab[lignetab] == 0) break; // inutile d'analyser plus loin sur l'echiquier
			
			if (lignetab == lignevar) {myconflit = true; if (debug) System.out.println(" CONFLIT LIGNE LIGNETAB "+lignetab+" LIGNEVAR "+lignevar);break;}
			if (coltab == colvar) {myconflit = true; if (debug) System.out.println(" CONFLIT COLONE COLTAB "+coltab+" COLVAR "+colvar);break;}
			
			deltacol = colvar - coltab;
			deltaligne = lignevar - lignetab;
			pente = (deltacol) / (deltaligne);
			if (debug) System.out.println(" PENTE "+pente+" DELTACOL:"+deltacol+" DELTALIGNE:"+deltaligne);
			if (Math.abs(pente) == 1) { myconflit = true ; break;}
			
			
		}
	
		if (debug) System.out.println("\n CONFLIT OUT "+myconflit);
		return myconflit;
	}
	
	public static void set (int refl,int refc) {	
		tab[refl] = refc;
		
		if (debug) {
		System.out.println("set ligne:"+refl+" colonne :"+refc);
		displayvector();
		}
		if (refl == 8) {
		sol++;
		System.out.println("\n SOLUTION "+sol);
		out.println("\n SOLUTION "+sol);
		display();
		// System.exit(0);		
		}	
	}
	
	public static void unset (int refl) {	
		tab[refl] = 0;
		if (debug) {
		System.out.println("UNSET refl =>"+refl);
		displayvector();
		}		
	}
	
	
	public static void displayvector() {
		int i=0;
		int j=0;
		System.out.println("display vector");
		for (i=0;i<9;i++) {
		System.out.println(" i:"+i+" tab[i]:"+tab[i]);
		
		}
	}
	
	public static void display() {
		
		int i=0;
		int j=0;
		int k=0;
		
		// pour presenter les unités d'absice
		System.out.println();out.println();
		System.out.print("  ");out.print("  ");
		for (j=1;j<9;j++) {System.out.print(j+" "); out.print(j+" "); }
		System.out.println(); out.println();
		
				for (i=1;i<9;i++) {
					System.out.print(i+" "); // pour presenter les unités d'ordonnée
					out.print(i+" "); // pour loggué les resultats
					if (tab[i] != 0) {
							j=tab[i];
							for (k=1;k<j;k++) {
								if ((i+k)%2 == 0) {
									System.out.print(". "); 
									out.print(". "); 
									}
									else {
									System.out.print("- ");
									out.print("- ");
									}
								}
							System.out.print(i+" "); out.print(i+" "); 
							for (k=j+1;k<9;k++) {
									if ((i+k)%2 == 0) {
									System.out.print(". "); 
									out.print(". "); 
									} else {
									System.out.print("- ");
									out.print("- ");
									}
								}
							}
							else {
							for (k=1;k<9;k++) {
								if ((i+k)%2 == 0) {
									System.out.print(". "); 
									out.print(". "); 
									}
									else {
									System.out.print("- ");
									out.print("- ");
									}
								}
							}
				System.out.println(); // passage a la ligne pour
				out.println();
		}
		
	}
	
	
	public static void place(int ligne) {
		
		//if (compteur++ > limit) System.exit(0);
		if (debug) {if (compteur++ > limit) System.exit(0);}

		if (debug) {
		System.out.println("\n PLACE ligne:"+ligne);
		display();
		}
		
		if (!conflit(ligne,1))  {set(ligne,1); place(ligne+1);}
		if (!conflit(ligne,2))  {set(ligne,2); place(ligne+1);}
		if (!conflit(ligne,3))  {set(ligne,3); place(ligne+1);}
		if (!conflit(ligne,4))  {set(ligne,4); place(ligne+1);}
		if (!conflit(ligne,5))  {set(ligne,5); place(ligne+1);}
		if (!conflit(ligne,6))  {set(ligne,6); place(ligne+1);}
		if (!conflit(ligne,7))  {set(ligne,7); place(ligne+1);}
		if (!conflit(ligne,8))  {set(ligne,8); place(ligne+1);}
		
		if (debug) System.out.println("ligne:"+ligne+" compteur :"+compteur);
		
		unset(ligne-1); // si cette ligne ne propose aucune suite c'est que la position precedente deja n'engendre pas de solution
		
	}
	
	public static void bla(String blabla) {
	System.out.println(blabla);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		
		if (args.length == 1 && args[0].equals("debug")) {
		debug = true;
		}
		
		// for(int i = 0; i < args.length; i++) {
            // System.out.println(i + " " +args[i]);
			// }
			bla("\n\n ===============Dame8=================== args=>"+args.length+" debug "+debug);
			// String fileName = "damesol.txt";
			try{
            //PrintWriter out = new PrintWriter( new FileWriter( fileName ) );
			out = new PrintWriter( new FileWriter( solFileName ) );
			out.println("Probleme des 8 DAMES");
			//out.close();
			}
			catch ( IOException error ) {
             System.err.println( "Error making file:" );
             System.err.println( "\t" + error );
			}

		//display();
		place(1);
		bla("Nombre de solutions "+sol+ " dans fichier "+solFileName);

		out.println("\n Nombre de solutions "+sol);
		

		out.close();
	
	
	}

}
